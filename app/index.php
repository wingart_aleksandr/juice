<!Doctype html>
<html lang="en">
    <!-- Head Start     ============================================ -->
    <head>
        <!-- Basic page needs     ============================================ -->
        <title>Главная страница</title>
        <meta charset="utf-8" />
        <meta name="author" content="" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <!-- Meta tags for social networks     ============================================-->
        <meta property="og:title" content="" />
        <meta property="og:type" content="" />
        <meta property="og:description" content="" />
        <meta property="og:url" content="" />
        <meta property="og:image" content="img/preview.jpg" />
        <!-- IE Support     "IE=edge","IE=11","IE=10","IE=9","IE=8"     ============================================ -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- Mobile specific metas     ============================================ -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- Disable conversion of telephone numbers     ============================================ -->
        <meta name="format-detection" content="telephone=no" />
        <!-- Implement Google Tag Manager     ============================================ -->
        <!-- Google Tag Manager -->
        <script>
            (function(w, d, s, l, i)
            {
                w[l] = w[l] || [];
                w[l].push(
                {
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MT4ZWVR');
        </script>
        <!-- End Google Tag Manager -->
        <!-- Favicon     ============================================-->
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="images/favicon-16.png" sizes="16x16" type="image/png" />
        <link rel="icon" href="images/favicon-32.png" sizes="32x32" type="image/png" />
        <link rel="icon" href="images/favicon-48.png" sizes="48x48" type="image/png" />
        <link rel="icon" href="images/favicon-62.png" sizes="62x62" type="image/png" />
        <link rel="icon" href="images/favicon-192.png" sizes="192x192" type="image/png" />
        <!-- Include Fonts (Icons)     ============================================ -->
        <link rel="stylesheet" media="screen" href="css/font.css" />
        <!-- Include Libs CSS     ============================================ -->
        <link rel="stylesheet" media="screen" href="css/reset.css" />
        <link rel="stylesheet" media="screen" href="css/bootstrap.css" />
        <!-- Theme CSS     ============================================ -->
        <link rel="stylesheet" media="screen" href="css/style.css" />
        <link rel="stylesheet" media="screen" href="css/responsive.css" />
        <link rel="stylesheet" media="screen" href="css/ie.css" /> </head>
    <!-- Head End       ============================================ -->
    <!-- Body Start     ============================================ -->
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MT4ZWVR" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
        <!--[if lte IE 9]><div class="browse-happy"><h3>You are using an <strong>outdated</strong> browser. Please<a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</h3></div><![end if]-->
        <div class="page_wrap">
            <!-- Preloader Start     ============================================ -->
            <div id="page-preloader">
                <span class="preloader"></span>
            </div>
            <!-- Preloader End       ============================================ -->
            <!-- Header Start     ============================================ -->
            <header class="header_fix" id="header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="header_container">
                                <div class="js-btn-menu">
                                    <span></span>
                                </div>
                                <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - --->
                                <div class="logo">
                                    <a class="scrollto" href="#box1">
                                        <img src="images/logo.png" alt="" />
                                    </a>
                                </div>
                                <!-- - - - - - - - - - - - - - End of Logo - - - - - - - - - - - - - - - --->
                                <!--  - - - - - - - - - - - - - Page Navigation - - - - - - - - - - - - - - - --->
                                <nav class="navigation_menu" id="primary_nav">
                                    <menu class="menu" id="menu">
                                        <li class="menu_item">
                                            <a class="menu_link gradient-text scrollto" href="#box2">Об акции</a>
                                        </li>
                                        <li class="menu_item">
                                            <a class="menu_link gradient-text scrollto" href="#box3">Призы</a>
                                        </li>
                                        <li class="menu_item">
                                            <a class="menu_link gradient-text" href="winners.html">Победители</a>
                                        </li>
                                        <li class="menu_item">
                                            <a class="menu_link gradient-text" href="about.html">Вопрос- ответ </a>
                                        </li>
                                        <li class="menu_item">
                                            <a class="menu_link gradient-text" href="feedback.html">Обратная&nbsp;связь</a>
                                        </li>
                                    </menu>
                                </nav>
                                <!--  - - - - - - - - - - - - - End of Page Navigation - - - - - - - - - - - - - - - --->
                                <!-- - - - - - - - - - - - - - login_box - - - - - - - - - - - - - - - --->
                                <div class="login_box">
                                    <a class="login_link" href="#">
                                        <img src="images/icons/login-icon.png" alt="" />
                                        <div class="gradient-text">Войти</div>
                                    </a>
                                </div>
                                <!-- - - - - - - - - - - - - - End of login_box - - - - - - - - - - - - - - - --->
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- Header End       ============================================ -->
            <!-- Content Start     ============================================-->
            <main id="content">
                <!-- - - - - - - - - - - - - - first_screen - - - - - - - - - - - - - - - --->
                <div class="first_screen bg_fon" style="background-image: url(images/new_bg.jpg);" id="box1">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="first_screen_box">
                                    <div class="first_screen_slogan">
                                        <img src="images/slogan.png" alt="" />
                                    </div>
                                    <div class="first_screen_wrap">
                                        <div class="time_container">
                                            <h5 class="fz20 time_title gradient-text">До&nbsp;окончания&nbsp;акции&nbsp;осталось</h5>
                                            <div class="js-clock time_box"></div>
                                        </div>
                                        <a class="first_screen_btn btn-xl btn-fon" href="javascript:;" data-popup="#add_check">
                                            <span class="gradient-text">ЗАГРУЗИТЬ&nbsp; ЧЕК</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="first_screen_img">
                                    <img src="images/first_screen.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- - - - - - - - - - - - - - End of first_screen - - - - - - - - - - - - - - - --->
                <!-- - - - - - - - - - - - - - second_screen - - - - - - - - - - - - - - - --->
                <section class="second_screen indent bg_fon" style="background-image: url(images/second_screen_fon.png);" id="box2">
                    <div class="container abs_box">
                        <div class="img_abs1">
                            <img src="images/img_abs1.png" alt="" />
                            <img class="d_none" src="images/img_abs1_mob.png" alt="" />
                        </div>
                        <div class="img_abs2">
                            <img src="images/img_abs2.png" alt="" />
                            <img class="d_none" src="images/img_abs2_mob.png" alt="" />
                        </div>
                        <div class="img_abs3_mob">
                            <img src="images/img_abs3_mob.png" alt="" />
                        </div>
                    </div>
                    <h2 class="gradient-text mb96 al_center">Об акции</h2>
                    <div class="container">
                        <div class="row mb90 js-carousel">
                            <div class="col-lg-3 offset-lg-1">
                                <figure class="figure_box">
                                    <div class="figure_img">
                                        <img src="images/icons/icon1.png" alt="" />
                                    </div>
                                    <figcaption>
                                        <a class="figure_box_slogan gradient-text" href="javascript:;">КУПИ “Я”</a>
                                        <p class="gradient-text"> В &nbsp;одном&nbsp; из &nbsp;
                                            <a class="d_i link-wr-bd" href="javascript:;" data-popup="#authorization_modal">
                                                <span class="link-bd gradient-text">участвующих</span>
                                                <br/>
                                                <span class="link-bd gradient-text">магазинов.</span>
                                            </a>
                                        </p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-lg-4">
                                <figure class="figure_box">
                                    <div class="figure_img">
                                        <img src="images/icons/icon2.png" alt="" />
                                    </div>
                                    <figcaption>
                                        <a class="figure_box_slogan gradient-text" href="javascript:;">ЗАРЕГИСТРИРУЙ</a>
                                        <p class="gradient-text">Загрузи&nbsp;чек&nbsp; на&nbsp; сайте &nbsp;
                                            <br>promo.ya-juice.ru </p>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-lg-3">
                                <figure class="figure_box">
                                    <div class="figure_img">
                                        <img src="images/icons/icon3.png" alt="" />
                                    </div>
                                    <figcaption>
                                        <a class="figure_box_slogan gradient-text" href="javascript:;">ВЫИГРАЙ</a>
                                        <p>
                                            <a class="link-bd ttu link-sm2 gradient-text scrollto" href="#box3">МОДНЫЕ&nbsp;ПРИЗЫ</a>
                                        </p>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div>
                            <a class="first_screen_btn btn-xl btn-fon2" href="javascript:;" data-popup="#add_check">
                                <span class="gradient-text">ЗАГРУЗИТЬ&nbsp; ЧЕК</span>
                            </a>
                        </div>
                    </div>
                </section>
                <!-- - - - - - - - - - - - - - End of second_screen - - - - - - - - - - - - - - - --->
                <!-- - - - - - - - - - - - - - third_screen - - - - - - - - - - - - - - - --->
                <section class="third_screen indent2 bg_fon" style="background-image: url(images/third_screen_fon.png);" id="box3">
                    <h2 class="title_ gradient-text mb60 al_center">ПРИЗЫ</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">
                                <div class="ban_box d_flex justify-content-between hide_767">
                                    <div class="ban_left ban_indent">
                                        <div class="title_fon" style="background-image:url(images/title_fon.png);">
                                            <h4 class="fz26 gradient-text">Главный приз</h4>
                                        </div>
                                        <h4 class="fz26 gradient-text">Яркое преображение от </h4>
                                        <img class="mb33" style="margin-top: 10px;" src="images/banner_brend.png" alt="" />
                                        <div class="ban_img_mob">
                                            <img src="images/banner_img_mob.png" alt="" />
                                            <div class="ban_footer fz14 gradient-text">*ICONFACE – школа профессионального макияжа</div>
                                        </div>
                                    </div>
                                    <div class="ban_right" style="background-image: url(images/banner_img.png);">
                                        <img class="d_none" src="images/banner_img.png" alt="" />
                                    </div>
                                </div>
                                <div class="ban_2col hide_767">
                                    <div class="ban_box view2 ban_indent2 clearfix">
                                        <div class="ban_left f_left">
                                            <div class="title_fon" style="background-image:url(images/title_fon2.png);">
                                                <h6 class="gradient-text">Ежедневный приз</h6>
                                            </div>
                                            <h4 class="mb18 fz26 gradient-text">Деньги на телефон</h4>
                                            <div class="ban_footer fz16 gradient-text">Ежедневно первым
                                                <br>100 участникам,
                                                <br> зарегистрировавшим чек.</div>
                                        </div>
                                        <div class="ban_right wrapper">
                                            <img src="images/banner_img2.png" alt="" />
                                        </div>
                                    </div>
                                    <div class="ban_box view2 ban_indent3 clearfix">
                                        <div class="ban_left f_left">
                                            <div class="title_fon" style="background-image:url(images/title_fon3.png);">
                                                <h6 class="gradient-text">Еженедельный приз</h6>
                                            </div>
                                            <h4 class="mb48 fz26 gradient-text">Розыгрыш 25
                                                <br>Beauty-боксов</h4>
                                            <div class="ban_footer fz12 gradient-text">**Beauty-бокс – набор косметики</div>
                                        </div>
                                        <div class="ban_right wrapper">
                                            <img src="images/banner_img3.png" alt="" />
                                            <img src="images/banner_brend2.png" alt="" />
                                        </div>
                                        <div class="ban_footer fz12 gradient-text">**Beauty-бокс – набор косметики</div>
                                    </div>
                                </div>
                                <div class="js-carousel2 owl-carousel">
                                    <div class="ban_box d_flex justify-content-between" data-mh="ban_box">
                                        <div class="ban_left ban_indent">
                                            <div class="title_fon" style="background-image:url(images/title_fon.png);">
                                                <h4 class="fz26 gradient-text">Главный приз</h4>
                                            </div>
                                            <h4 class="fz26 gradient-text">Яркое преображение от </h4>
                                            <img class="mb33" src="images/banner_brend.png" alt="" />
                                            <div class="ban_img_mob">
                                                <img src="images/banner_img_mob.png" alt="" />
                                                <div class="ban_footer fz14 gradient-text">*ICONFACE – школа профессионального макияжа</div>
                                            </div>
                                        </div>
                                        <div class="ban_right" style="background-image: url(images/banner_img.png);">
                                            <img class="d_none" src="images/banner_img.png" alt="" />
                                        </div>
                                    </div>
                                    <div class="ban_box view2 ban_indent2 clearfix" data-mh="ban_box">
                                        <div class="ban_left f_left">
                                            <div class="title_fon" style="background-image:url(images/title_fon2.png);">
                                                <h6 class="gradient-text">Ежедневный приз</h6>
                                            </div>
                                            <h4 class="mb18 fz26 gradient-text">Деньги не телефон</h4>
                                            <div class="ban_footer fz16 gradient-text">Ежедневно первым
                                                <br>100 участникам,
                                                <br> зарегистрировавших чек.</div>
                                        </div>
                                        <div class="ban_right wrapper">
                                            <img src="images/banner_img2.png" alt="" />
                                        </div>
                                    </div>
                                    <div class="ban_box view2 ban_indent3 clearfix" data-mh="ban_box">
                                        <div class="ban_left f_left">
                                            <div class="title_fon" style="background-image:url(images/title_fon3.png);">
                                                <h6 class="gradient-text">Еженедельный приз</h6>
                                            </div>
                                            <h4 class="mb48 fz26 gradient-text">Розыгрыш 25
                                                <br>Beauty-боксов</h4>
                                            <div class="ban_footer fz12 gradient-text">**Beauty-бокс – набор косметики</div>
                                        </div>
                                        <div class="ban_right wrapper">
                                            <img src="images/banner_img3.png" alt="" />
                                            <img src="images/banner_brend2.png" alt="" />
                                        </div>
                                        <div class="ban_footer fz12 gradient-text">**Beauty-бокс – набор косметики</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container abs_box">
                        <img class="img_abs3" src="images/img_abs3.png" alt="" />
                    </div>
                </section>
                <!-- - - - - - - - - - - - - - End of third_screen - - - - - - - - - - - - - - - --->
            </main>
            <!-- Content End       ============================================-->
            <!-- Footer Start     ============================================ -->
            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="footer_container">
                                <div class="footer_tel">
                                    <a class="gradient-text" href="tel:88005007737">8&nbsp;800&nbsp;500&nbsp;77&nbsp;37</a>
                                    <p class="gradient-text">Телефон&nbsp;горячей&nbsp;линии</p>
                                </div>
                                <ul class="footer_list">
                                    <li>
                                        <a class="gradient-text" href="javascript:;" target="_blank">Правила&nbsp;акции</a>
                                    </li>
                                    <li>
                                        <a class="gradient-text" href="javascript:;" target="_blank">Пользовательское&nbsp;соглашение</a>
                                    </li>
                                </ul>
                                <div class="footer_social">
                                    <h6 class="social_title gradient-text">Поделиться:</h6>
                                    <ul>
                                        <li>
                                            <a href="javascript:;">
                                                <img src="images/icons/social-icon.png" alt="" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <img src="images/icons/social-icon2.png" alt="" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="footer_copyright gradient-text">
                                    <span class="txt">©&nbsp;</span>2018&nbsp;ООО&nbsp;«ПепсиКо&nbsp;Холдингс»&nbsp;Все&nbsp;права&nbsp;защищены</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- - - - - - - - - - - - - - End of footer_container - - - - - - - - - - - - - - - --->
            </footer>
            <!-- Footer End       ============================================ -->
            <!-- Popup Start     ============================================-->
            <div class="d_none">
                <div class="arcticmodal_container_box" id="request_popup">
                    <div class="arcticmodal-close">&times;</div>
                    <div>Для отмены заявки, свяжитесь с нами по телефону +7 (819) 987-90-90</div>
                </div>
                <div class="arcticmodal_container_box modal_box3 modal_indent2" id="authorization_modal">
                    <div class="modal_abs_img">
                        <img src="images/modal_abs_img4.png" alt="" />
                    </div>
                    <div class="modal_abs_img4">
                        <img src="images/modal_abs_img5.png" alt="" />
                        <img class="d_none" src="images/modal_abs_img5_mob.png" alt="" />
                    </div>
                    <div class="arcticmodal-close">&times;</div>
                    <h3 class="mb7 al_center">Ошибка при загрузке чека</h3>
                    <div class="al_center gradient-text fz18">Чек сфотографирован не полностью</div>
                </div>
                <div class="arcticmodal_container_box modal_box4 modal_indent4" id="add_check">
                    <div class="arcticmodal-close">&times;</div>
                    <h3 class="mb15 gradient-text" data-gradient-text-range="d7bf6c, d7bf6c, faefc5">Загрузить чек</h3>
                    <div class="mb20">
                        <div class="fz18">
                            <span class="gradient-text" data-gradient-text-range="d7bf6c, d7bf6c, faefc5">Период регистрации чеков:</span>
                        </div>
                        <div class="fz18">
                            <span class="gradient-text" data-gradient-text-range="d7bf6c, d7bf6c, faefc5">с 1 сентября по 31 октября 2018 года.</span>
                        </div>
                    </div>
                    <div id="dropzone">
                        <form class="dropzone needsclick" action="upload.php">
                            <div class="fallback">
                                <input name="file" type="file" multiple="multiple" /> </div>
                            <div class="dz-message needsclick">
                                <span class="gradient-text" data-gradient-text-range="d7bf6c, d7bf6c, faefc5">Перетащите файл в эту область</span>
                            </div>
                        </form>
                    </div>
                    <a class="btn-sm btn-red btn-radius" href="#">
                        <span class="gradient-text" data-gradient-text-range="d7bf6c, d7bf6c, faefc5">Загрузить</span>
                    </a>
                </div>
            </div>
            <!-- Popup End       ============================================-->
        </div>
        <!-- Include Libs     ============================================ -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/jquery-migrate-3.0.1.min.js"></script>
        <!-- Include Libs End       ============================================ -->
        <!-- Scripts Init Plugins & Core START     ============================================ -->
        <script src="js/script.init.js"></script>
        <script src="js/script.core.js"></script>
        <script type='text/javascript'>
        Dropzone.autoDiscover = false;
        $(".dropzone").dropzone({
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;    
                
                $.ajax({
                    type: 'POST',
                    url: 'upload.php',
                    data: {name: name,request: 2},
                    sucess: function(data){
                        console.log('success: ' + data);
                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
        </script>
        <!-- Scripts Init Plugins & Core END       ============================================ -->
    </body>
    <!-- Body End       ============================================ -->
</html>