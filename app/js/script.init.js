//  /*================================================>
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/





		var styler 		= $(".styler"),
		    popup 		= $("[data-popup]"),
		    mh 		= $("[data-mh]"),
			gradientTxt = $('.gradient-text'),
			timer		= $('.js-clock'),
			carousel 	= $('.js-carousel'),
			carousel2 	= $('.js-carousel2'),
			tooltip 	= $('.tooltip'),
			dropzone 	= $('.dropzone'),
			scroll 		= $('.js-scroll');

			if(styler.length){
					include("plugins/formstyler/formstyler.js");
					// includeCss("plugins/formstyler/formstyler.css");
			}
			if(popup.length){
					include("plugins/arcticmodal/jquery.arcticmodal.js");
					// includeCss("plugins/arcticmodal/jquery.arcticmodal.css");
			}
			if(mh.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(gradientTxt.length){
					include("plugins/gradienttext/jquery.gradient.text.js");	
			}
			if(timer.length){
					include("plugins/timer/jquery.countdown.min.js");
			}
			
			if(scroll.length){
					include("plugins/mCustomScrollbar/jquery.mCustomScrollbar.js");
					// includeCss("plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css");
			}
			if(carousel.length || carousel2.length || carousel2.length){
					include("plugins/owl-carousel/owl.carousel.min.js");
					// includeCss("plugins/owl-carousel/owl.carousel.min.css");	
			}
			if(tooltip.length){
					include("plugins/tooltipster-master/tooltipster.bundle.min.js");
					// includeCss("plugins/tooltipster-master/tooltipster.bundle.min.css");	
			}
			// if(dropzone.length){
					include("plugins/dropzone/dropzone.js");
					// includeCss("plugins/dropzone/dropzone.css");	
			// }

					include("plugins/modernizr.js");



			function include(url){

					document.write('<script src="'+ url + '"></script>');

			}

			function includeCss(href){

					var href = '<link rel="stylesheet" media="screen" href="' + href +'">';

					if($("[href*='style.css']").length){

						$("[href*='style.css']").before(href);

					}
					else{

						$("head").prepend(href);

					}

			}



		$(document).ready(function(){


			/* ------------------------------------------------
			Owl Carousel START
			------------------------------------------------ */

					if(carousel.length || carousel2.length){
						
						$(window).on('resize', function() {
								owl();	
								owl2();	
						});

						function owl(){
							if($(window).width() <= 991){
								carousel.addClass('owl-carousel');
								carousel.owlCarousel({
									items: 1,
									loop: false,
									nav: false,
									dots: true,
								});
							}
							else if($(window).width() >= 992){
								carousel.removeClass('owl-carousel');
								carousel.owlCarousel('destroy');
							}
						}
						owl();

						function owl2(){
							if($(window).width() <= 767){
								carousel2.owlCarousel({
									items: 1,
									loop: false,
									nav: false,
									//autoHeight: true,
									dots: true,
								});
							}
							else if($(window).width() >= 768){
								carousel2.owlCarousel('destroy');
							}
						}
						owl2();
					}

			/* ------------------------------------------------
			Owl Carousel END
			------------------------------------------------ */


			/* ------------------------------------------------
			Clock START
			------------------------------------------------ */

					if(timer.length){

						timer.countdown('2018/08/20', function(event) {
							var $this = $(this).html(event.strftime(
								''
								+ '<div class="timer-main" style="background: rgb(39,129,68);background: -moz-linear-gradient(left, rgba(39,129,68,1) 0%, rgba(37,119,66,1) 100%);background: -webkit-linear-gradient(left, rgba(39,129,68,1) 0%,rgba(37,119,66,1) 100%);background: linear-gradient(to right, rgba(39,129,68,1) 0%,rgba(37,119,66,1) 100%);"><div class="timer-time gradient-text">%d</div> <div class="timer-text gradient-text">Дней</div></div>'
								+ '<div class="timer-main" style="background: rgb(37,118,66);background: -moz-linear-gradient(left, rgba(37,118,66,1) 0%, rgba(34,107,62,1) 100%);background: -webkit-linear-gradient(left, rgba(37,118,66,1) 0%,rgba(34,107,62,1) 100%);background: linear-gradient(to right, rgba(37,118,66,1) 0%,rgba(34,107,62,1) 100%);"><div class="timer-time gradient-text">%H</div> <div class="timer-text gradient-text">Часов</div></div>'
								+ '<div class="timer-main" style="background: rgb(34,106,62);background: -moz-linear-gradient(left, rgba(34,106,62,1) 0%, rgba(33,96,59,1) 100%);background: -webkit-linear-gradient(left, rgba(34,106,62,1) 0%,rgba(33,96,59,1) 100%);background: linear-gradient(to right, rgba(34,106,62,1) 0%,rgba(33,96,59,1) 100%);"><div class="timer-time gradient-text">%M</div> <div class="timer-text gradient-text">Минут</div></div>'
								// + '<div class="timer-main"><div class="timer-time" style="color: #3d2747;">%S</div> <div class="timer-text">seconds</div></div>'
							));
						}).on('finish.countdown', timecallback);
						function timecallback(){
							// $('body .timer-time.gradient-text,body .timer-text.gradient-text').gradientText({
							// 	colors: ['#d7bf6c', '#f0e1ab', '#faefc5']
							// });

							$('body .timer-time.gradient-text,body .timer-text.gradient-text').css({
								'background': 'linear-gradient(45deg, #d7bf6c 33%, #d7bf6c 66%, #faefc5)',
								'-webkit-background-clip': 'text',
								'-webkit-text-fill-color': 'transparent'
							});
						}
					}

			/* ------------------------------------------------
			Clock END
			------------------------------------------------ */


			/* ------------------------------------------------
			gradientText START
			------------------------------------------------ */

					if(gradientTxt.length){
						// gradientTxt.gradientText({
						// 	colors: ['#d7bf6c', '#f0e1ab', '#faefc5']
						// });

						gradientTxt.attr('data-gradient-text-range', 'd7bf6c, d7bf6c, faefc5')
					}

			/* ------------------------------------------------
			gradientText END
			------------------------------------------------ */


			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							selectSmartPositioning: true,
							fileBrowse: '',
							filePlaceholder: 'Прикрепить файл'
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */


			/* ------------------------------------------------
			SCROLL START
			------------------------------------------------ */

					if(scroll.length){
						scroll.mCustomScrollbar({
							mouseWheelPixels: 100
						});
					}

			/* ------------------------------------------------
			SCROLL END
			------------------------------------------------ */


			/* ------------------------------------------------
			tooltip START
			------------------------------------------------ */

					if (tooltip.length){
						tooltip.tooltipster({
						    contentCloning: true,
						    // trigger: 'click'
						});
					}

			/* ------------------------------------------------
			tooltip END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal({
						    // 	afterOpen: function(){
						    // 		if($(this).find('.dropzone')){
						    // // 			$('.dropzone').dropzone({ 
										// // 	url: "/file/post"
										// // });
										// // Dropzone.autoDiscover = false;
										// // $(".dropzone").dropzone({
										// //     addRemoveLinks: true,
										// //     removedfile: function(file) {
										// //         var name = file.name;    
										        
										// //         $.ajax({
										// //             type: 'POST',
										// //             url: 'plugins/dropzone/upload.php',
										// //             data: {name: name,request: 2},
										// //             sucess: function(data){
										// //                 console.log('success: ' + data);
										// //             }
										// //         });
										// //         var _ref;
										// //         return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
										// //     }
										// // });
						    // 		}
						    // 	}
						    });
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */




		});


		$(window).load(function(){
			
		});




//  /*================================================>
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
