;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;

			self.mainMenu();
			self.firstScreen();
			self.accordion();
			// self.navFixed();
			self.scrollto();
			self.winnersSearch();
			self.popupUploadFile();

		},

		windowLoad: function(){

			var self = this;

			self.preloader();
			self.anchor();

		},

		/**
	    **  PopupUploadFile
	    **/

		popupUploadFile: function(){
			function check(){
				var btnPhoto = $('.add-photo'),
					btnSend = $('.send-check'),
					btnDownload = $('.download-btn');

				if($('.dz-preview').length){
					if($('.dz-preview').length < 5){
						btnPhoto.addClass('show');
						btnSend.addClass('show');
						btnDownload.addClass('hide');
					}
					if($('.dz-preview').length == 5){
						btnPhoto.removeClass('show');
					}
				}
			}
			check();

			setInterval(function(){
				check();
			},1000)

		},

		/**
	    **  Winners Search
	    **/

		winnersSearch: function(){

			jQuery.expr[':'].Contains = function(a,i,m){
			    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
			};

			var input = $('.winners_search>input');

			input.on('keyup keypress blur change', function(){

				var inpVal = $(this).val(),
					listItem = $('.accordion-box').find('.js-winerFilter'),
					section = $('.accordion-box').find('dd');

				if(inpVal){
					
					var $matches = $('.accordion-box').find('.js-winerFilter:Contains(' + inpVal + ')').closest('tr');
					$('.accordion-box tbody>tr').not($matches).hide();
					$matches.show();

					section.each(function(index, el){
						
						console.log($(el).find('tbody>tr:visible').length)

						if($(el).find('tbody>tr:visible').length){
							$(el).find('.winners_error').hide();
							$(el).find('thead').show();
						}
						else{
							$(el).find('.winners_error').show();
							$(el).find('thead').hide();
						}
					});
				}
				else{
					$('.accordion-box tbody>tr').slideDown();
					$('.accordion-box').find('.winners_error').hide();
					$('.accordion-box').find('thead').show();
				}

			});
			$('.accordion-box').on('click', 'dt:not(.active)', function(){
				input.change();
			});


		},

		/**
	    **  Anchor
	    **/

		anchor: function(){
			
			var hash = window.location.hash;
			if(hash !== ""){
				var	header = $("header").height(),
					offset =  $(hash).offset().top;
				if(!hash) return;

				$('html, body').stop().animate({   
					
				 	scrollTop: offset - header
				
				}, 800);
			}

		},

		/**
	    **  Scroll To
	    **/

	    scrollto: function(){

	      var self = this;
	      var headerH = $('header').outerHeight();

	      $("a.scrollto").click(function() {
	          var elementClick = $(this).attr("href")
	          var destination = $(elementClick).offset().top;
	          jQuery("html:not(:animated),body:not(:animated)").animate({
	            scrollTop: destination - headerH
	          }, 800);
	          // return false;
	        });
	    },

		/**
		**	Accordion
		**/

		accordion: function(){
			var self = this;
			
				self.accordion = $('.accordion-box');
				self.accordionDt = self.accordion.find('dt.active').length ? self.accordion.children('dt.active') : self.accordion.children('dt:first').addClass('active');

				self.accordion.children('dt').not(self.accordionDt).next().hide();

				if(self.accordion.hasClass('toggle')){
					self.accordion.on('click', 'dt', function(){
						$(this).toggleClass('active').next().stop().slideToggle(400);
		            });
				}
				else{
					self.accordion.on('click', 'dt', function(){
						$('.accordion-box').find('dt').removeClass('active');
						$('.accordion-box').find('dd').slideUp(400);
						$(this).addClass('active').next().stop().slideDown(400);
		            });
				}

		},

		/**
		**	Main Menu
		**/

		mainMenu: function(){

			$('.js-btn-menu').on('click', function(){
				$(this).toggleClass('active');
				$('.navigation_menu').toggleClass('open');
			});

		},
		
		/**
		**	firstScreen
		**/

		firstScreen: function(){
			var wH = $(window).height(),
				headerH = $('header').outerHeight(),
				footerH = $('footer').outerHeight();

			if($(window).width() >= 992){

				if($('header').length && !$('footer').length){
					$('.first_screen').css({
						'height': wH - headerH
					})
					// console.log('header')
				}
				else if($('header').length && $('footer').length){
					$('.first_screen').css({
						'height': wH - headerH - footerH
					});
					// console.log('header, footer')
				}
				else if($('footer').length && !$('header').length){
					$('.first_screen').css({
						'height': wH - footerH
					})
					// console.log('footer')
				}
				else{
					$('.first_screen').addClass('full');
					// console.log('not')
				}
				
			}

		},

		/**
		**	Nav fixed
		**/

		navFixed: function(){
			var footerH = $('footer').outerHeight();



				if($('.footer_fix').length){
					$('body').css({
						'padding-bottom': footerH
					})
					//console.log('header')
				}

				$(window).resize(function(){
					var footerH = $('footer').outerHeight();

			    	$('body').css({
						'padding-bottom': footerH
					})
					//console.log('header')
			    });



		},

		/**
		**	Preloader
		**/

		preloader: function(){

			var self = this;

			self.preloader = $('#page-preloader');
	        self.spinner   = self.preloader.find('.preloader');

		    self.spinner.fadeOut();
		    self.preloader.delay(350).fadeOut('slow');
		},

	}


	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).load(function(){

		Core.windowLoad();

	});

})(jQuery);